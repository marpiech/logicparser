# -----------------------------------------------------------------------------
# logical.py
#
# A simple logical equation simplifier 
# Mateusz Jarkiewicz & Marcin Piechota
# -----------------------------------------------------------------------------

import sys
sys.path.insert(0,"ply-3.4")

globalResult = 1

if sys.version_info[0] >= 3:
    raw_input = input

tokens = (
    'SYMBOL','NUMBER',
    )

literals = ['&','|', '(',')']

# Tokens

#t_SYMBOL = r'[a-zA-Z]'
t_NUMBER = r'[0-1]'

t_ignore = " \t"

def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")
    
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)
    
# Build the lexer
import ply.lex as lex
lex.lex()

# Parsing rules

precedence = (
    ('left','&','|'),
    ('right','UMINUS'),
    )

# dictionary of names
names = { }

def p_statement_expr(p):
    'statement : expression'
    global globalResult
    globalResult = str(p[1])
    #return p[1]
    #print("TADA" + str(p[1]))

def p_expression_binop(p):
    '''expression : expression '|' expression
                  | expression '&' expression'''
    if p[2] == '|'  : p[0] = int(p[1]) | int(p[3])
    elif p[2] == '&': p[0] = int(p[1]) & int(p[3])

def p_expression_uminus(p):
    "expression : '-' expression %prec UMINUS"
    p[0] = -p[2]

def p_expression_group(p):
    "expression : '(' expression ')'"
    p[0] = p[2]

def p_expression_number(p):
    "expression : NUMBER"
    p[0] = p[1]

def p_error(p):
    if p:
        print("Syntax error at '%s'" % p.value)
    else:
        print("Syntax error at EOF")

import ply.yacc as yacc
yacc.yacc()
import re
import qm

while 1:
    try:
        s = raw_input('calc > ')
    except EOFError:
        break
    if not s: continue
    s = re.sub(r"([a-zA-Z])([a-zA-Z])", r"\1&\2", s, flags=re.LOCALE) #inserting '&' between letters
    s = re.sub(r"([a-zA-Z])([a-zA-Z])", r"\1&\2", s, flags=re.LOCALE) #inserting again because it is greedy
    print "YOUR PHRASE: " + s
    letters = list(set(re.findall(r"([a-zA-Z])", s))) #finding unique letters
    letters.sort(reverse=True) #sorting in decreasing order, because little endian
    minoms = list()
    for num in range(0,pow(2, len(letters))): #number of 01 kombinacje
        logic = s
        for bit in range(0, len(letters)): #podmiana litery na 0lub1 
            logicValue = str(int(num&(pow(2,bit)) > 0))
            logic = re.sub(letters[bit], logicValue, logic)
        print "NUM:" + str(num)
	print logic
	result = yacc.parse(logic) #parsowanie
        if (globalResult == "1"):
             minoms.append(num)
	print globalResult
    letters.sort(reverse=True)
    qmo = qm.QM(letters)
    #print "MINOMS"
    #print minoms
    print qmo.get_function(qmo.solve(minoms,[])[1])

#esspresso porownac
