# -----------------------------------------------------------------------------
# logical.py [logicparser]
#
# A simple logical equation simplifier 
# (c) Mateusz Jarkiewicz & Marcin Piechota
# -----------------------------------------------------------------------------

from time import time
import subprocess
import sys
sys.path.insert(0,"ply-3.4")

globalResult = 1

if sys.version_info[0] >= 3:
    raw_input = input

tokens = (
    'SYMBOL','NUMBER', 'IMPL', 'EQUIV',
    )

literals = ['&','|', '(',')','=','>','^','~']

# Tokens
t_NUMBER = r'[0-1]'
t_IMPL = r'=>'
t_EQUIV = r'<=>'
t_ignore = " \t"

def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")
    
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)
    
# Build the lexer
import ply.lex as lex
lex.lex()

# Parsing rules

precedence = (
    ('left','&','|','^','IMPL','EQUIV'),
    ('right','UMINUS','~'),
    )

# dictionary of names
names = { }

def p_statement_expr(p):
    'statement : expression'
    global globalResult
    globalResult = str(p[1])
    #return p[1]
    #print("TADA" + str(p[1]))

def negate(x):
    if (x != 0):
        return 0
    else:
        return 1

def p_expression_binop(p):
    '''expression : expression '|' expression
                  | expression '&' expression
                  | expression '^' expression
                  | expression IMPL expression
                  | expression EQUIV expression'''
    if p[2] == '|'  : p[0] = int(p[1]) | int(p[3])
    elif p[2] == '&': p[0] = int(p[1]) & int(p[3])
    elif p[2] == '^' : p[0] = int(p[1]) ^ int(p[3]) #xor
    elif p[2] == '=>' : p[0] = negate(int(p[1])) | int(p[3]) #implikacja
    elif p[2] == '<=>' : p[0] = (negate(int(p[1])) | int(p[3])) & (negate(int(p[3])) | int(p[1])) #rownowaznosc

def p_expression_neg(p):  #negacja
    "expression : '~' expression %prec UMINUS"
    p[0] = negate(p[2])

def p_expression_uminus(p):
    "expression : '-' expression %prec UMINUS"
    p[0] = negate(p[2])

def p_expression_group(p):
    "expression : '(' expression ')'"
    p[0] = p[2]

def p_expression_number(p):
    "expression : NUMBER"
    p[0] = p[1]

def p_error(p):
    if p:
        print("Syntax error at '%s'" % p.value)
    else:
        print("Syntax error at EOF")

import ply.yacc as yacc
yacc.yacc()
import re
import qm

while 1:
    try:
        s = raw_input('calc > ')
    except EOFError:
        break
    if not s: continue
    s = re.sub(r"([a-zA-Z])([a-zA-Z])", r"\1&\2", s, flags=re.LOCALE) #inserting '&' between letters
    s = re.sub(r"([a-zA-Z])([a-zA-Z])", r"\1&\2", s, flags=re.LOCALE) #inserting again because it is greedy
    print "YOUR PHRASE: " + s
    letters = list(set(re.findall(r"([a-zA-Z])", s))) #finding unique letters
    letters.sort(reverse=True) #sorting in decreasing order, because little endian
    f = open('workfile', 'w')
    f.write(".i " + str(len(letters)) + "\n")
    f.write(".o " + "1" + "\n")
    f.write(".lib " + " ".join(letters) + "\n")
    minoms = list()
    for num in range(0,pow(2, len(letters))): #number of 01 kombinacje
        logic = s
        for bit in range(0, len(letters)): #podmiana litery na 0lub1 
            logicValue = str(int(num&(pow(2,bit)) > 0))
            f.write(logicValue)
            logic = re.sub(letters[bit], logicValue, logic)
        #print "NUM:" + str(num)
	#print logic
	result = yacc.parse(logic) #parsowanie
        if (globalResult == "1"):
             minoms.append(num)
        f.write(" " + globalResult + "\n")
	#print globalResult
    letters.sort(reverse=True)
    f.write(".e" + "\n")
    f.close()
    t0 = time()
    qmo = qm.QM(letters)
    print qmo.get_function(qmo.solve(minoms,[])[1])
    t1 = time()
    out = subprocess.check_output(["espresso-2.3.3/espresso", "workfile"])
    t2 = time()
    members = 0
    ex = []
    for line in out.split("\n"):
        if line == ".e":
		break
        items = line.split(" ")
        if (items[0] == ".lib"):
            1
        elif (items[0] == ".i"):
            1
        elif (items[0] == ".o"):
            1
        elif (items[0] == ".p"):
            members = int(items[1])
        else:
            members = members - 1
            counter = 0
            inletters = []
            for x in list(items[0]):
                if (x == "1"):
                    inletters.append("" + letters[counter])
                if (x == "0"):
                    inletters.append("~" + letters[counter])
                counter = counter + 1
            ex.append("(" + " AND ".join(inletters) + ")")
    print " OR ".join(ex)
    print "QM:\t" + str(t1 - t0) + "\tespresso:\t" + str(t2-t1)
#esspresso porownac
